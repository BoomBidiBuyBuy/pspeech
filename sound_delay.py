import numpy as np
import scipy.io.wavfile as wav
import multiprocessing as mp
import time

def proc_q(q):
    import sounddevice as sd

    while True:
        d = queue.get()
        sd.play(d)
        sd.wait()
    # while
# def

fs=44100
duration = 15  # seconds

queue = mp.Queue(maxsize=1)
p = mp.Process(target=proc_q, args=(queue,))
p.start()

import sounddevice as sd

while True:
    myrecording = sd.rec(duration * fs, samplerate=fs, channels=2, dtype='float64')
    print "Recording Audio for %s seconds" %(duration)
    sd.wait()
    print "Audio recording complete , Playing recorded Audio"
    print(len(myrecording), type(myrecording))
    queue.put(myrecording)
    #sd.play(myrecording, fs)
    #sd.wait()
# while
    
sd.play(myrecording, fs)
sd.wait()
print "Play Audio Complete"
